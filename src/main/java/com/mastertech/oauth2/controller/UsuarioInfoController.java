package com.mastertech.oauth2.controller;

import com.mastertech.oauth2.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/userinfo")
public class UsuarioInfoController {

    @GetMapping
    public Usuario mostrarInfo(@AuthenticationPrincipal Usuario usuario) {
        return usuario;
    }
}
